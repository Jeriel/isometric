use sdl2::event::Event;
use sdl2::init;
use sdl2::keyboard::Keycode;
use sdl2::EventPump;
use std::time::SystemTime;

use engine::renderer::*;

pub enum GraphicsApi {
    OpenGl,
    Vulkan,
}

pub enum Key {
    Up = 0,
    Down = 1,
    Left = 2,
    Right = 3,
    Escape = 4,
    Max = 5,
}

pub struct InputState {
    event_pump: EventPump,
    keys_down: [bool; Key::Max as usize],
    should_close: bool,
}

impl InputState {
    fn update_events(&mut self) {
        for event in self.event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => self.should_close = true,
                Event::KeyDown { keycode, .. } => {
                    if let Some(code) = keycode {
                        match code {
                            Keycode::Escape => {
                                self.keys_down[Key::Escape as usize] = true;
                            }
                            Keycode::Up => {
                                self.keys_down[Key::Up as usize] = true;
                            }
                            Keycode::Left => {
                                self.keys_down[Key::Left as usize] = true;
                            }
                            Keycode::Down => {
                                self.keys_down[Key::Down as usize] = true;
                            }
                            Keycode::Right => {
                                self.keys_down[Key::Right as usize] = true;
                            }
                            _ => {}
                        }
                    }
                }

                Event::KeyUp { keycode, .. } => {
                    if let Some(code) = keycode {
                        match code {
                            Keycode::Up => {
                                self.keys_down[Key::Up as usize] = false;
                            }
                            Keycode::Left => {
                                self.keys_down[Key::Escape as usize] = false;
                            }
                            Keycode::Down => {
                                self.keys_down[Key::Escape as usize] = false;
                            }
                            Keycode::Right => {
                                self.keys_down[Key::Escape as usize] = false;
                            }
                            _ => {}
                        }
                    }
                }
                _ => {}
            }
        }
    }

    pub fn key_down(&self, keycode: Key) -> bool {
        self.keys_down[keycode as usize]
    }
}

pub struct Engine {
    _graphics_api: GraphicsApi,
    pub renderer: Renderer,
    pub input_state: InputState,
}

impl Engine {
    pub fn init(graphics_api: GraphicsApi) -> Result<Engine, String> {
        match graphics_api {
            GraphicsApi::OpenGl => {
                // Initialize sdl2
                let sdl_context = init().expect("SDL initialization failed");
                let video_subsystem = sdl_context
                    .video()
                    .expect("Couldn't get SDL video subsystem");

                // Get event pump
                let event_pump = sdl_context
                    .event_pump()
                    .expect("Failed to get SDL event pump");

                // Initialize the Render
                let renderer = Renderer::create(video_subsystem, 800, 600);

                Ok(Engine {
                    _graphics_api: graphics_api,
                    renderer: renderer,
                    input_state: InputState {
                        event_pump: event_pump,
                        keys_down: [false; Key::Max as usize],
                        should_close: false,
                    },
                })
            }
            _ => Err(String::from(
                "Unsupported graphics API. Only OpenGl is currently supported",
            )),
        }
    }

    pub fn run<F>(&mut self, update_function: &mut F)
    where
        F: FnMut(f64, &mut Engine),
    {
        let mut current_time = SystemTime::now();

        'main: loop {
            let start = SystemTime::now();
            let delta = start.duration_since(current_time).unwrap();
            current_time = start;
            let dt = delta.as_secs() as f64 + delta.subsec_nanos() as f64 * 1e-9;

            // Update the game
            self.input_state.update_events();
            update_function(dt, self);

            if self.input_state.should_close {
                break 'main;
            }
        }
    }

    pub fn exit(&mut self) {
        self.input_state.should_close = true;
    }
}
