use sdl2::rect::Rect;
use sdl2::render::WindowCanvas;
use sdl2::VideoSubsystem;

use engine::resource_manager::TextureManager;

pub struct Renderer {
    pub close: bool,
    window_size: (u32, u32),
    pub canvas: WindowCanvas,
}

impl Renderer {
    pub fn create(
        video_subsystem: VideoSubsystem,
        window_width: u32,
        window_height: u32,
    ) -> Renderer {
        let window = video_subsystem
            .window("Isometric Rust", window_width, window_height)
            .position_centered()
            .opengl()
            .build()
            .expect("Failed to create window");

        let canvas = window.into_canvas().build().unwrap();

        Renderer {
            close: false,
            window_size: (window_width, window_height),
            canvas: canvas,
        }
    }
}

impl Renderer {
    pub fn copy_texture(&mut self, path: &str, rect: Rect) {
        let texture_creator = self.canvas.texture_creator();
        let mut texture_manager = TextureManager::new(&texture_creator);
        let texture = texture_manager.load(path).unwrap();

        self.canvas.copy(&texture, None, rect);
    }
}
