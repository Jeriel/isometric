use engine::core::{Engine, Key};
use engine::renderer::*;
use sdl2::image::LoadTexture;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Texture;

use std::path::Path;

//use engine::resource_manager::TextureManager;
use game::level;
use utils::math::coordinates;

const TILE_WIDTH: i32 = 50;
const BORDER_OFFSET: coordinates::Point = coordinates::Point { x: 250, y: 50 };
const FLOOR_GRAPHICS_WIDTH: u32 = 103;
const FLOOR_GRAPHICS_HEIGH: u32 = 53;
const WALL_GRAPHICS_WIDTH: u32 = 98;
const WALL_GRAPHICS_HEIGH: u32 = 103;

pub struct GameState {}

impl GameState {
    pub fn create() -> GameState {
        GameState {}
    }

    pub fn initialize(&mut self, renderer: &mut Renderer) {
        let texture_creator = renderer.canvas.texture_creator();
        //let mut texture_manager = TextureManager::new(&texture_creator);

        //let teste = texture_manager.load("assets/hero/1.png");

        let floor = texture_creator
            .load_texture("assets/floor.png")
            .expect("Couldn't load image");
        let block = texture_creator
            .load_texture("assets/block.png")
            .expect("Couldn't load image");

        //textures.insert("teste", teste.unwrap());
        renderer.canvas.set_draw_color(Color::RGB(204, 204, 204));
        renderer.canvas.clear();

        self.render_scene(renderer, &floor, &block);

        renderer.canvas.present();
    }

    pub fn update(&mut self, dt: f64, engine: &mut Engine) {
        if engine.input_state.key_down(Key::Escape) {
            engine.exit();
        }

        let input_state = &engine.input_state;

        if input_state.key_down(Key::Up) {
            println!("Up pressed!");
        }
        if input_state.key_down(Key::Left) {
            println!("Left pressed!");
        }
        if input_state.key_down(Key::Down) {
            println!("Down pressed!");
        }
        if input_state.key_down(Key::Right) {
            println!("Right pressed!");
        }

        let renderer = &mut engine.renderer;

        let texture_creator = renderer.canvas.texture_creator();

        let floor = texture_creator
            .load_texture("assets/floor.png")
            .expect("Couldn't load image");
        let block = texture_creator
            .load_texture("assets/block.png")
            .expect("Couldn't load image");

        self.render_scene(renderer, &floor, &block);

        //::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }

    fn render_scene(&mut self, renderer: &mut Renderer, floor: &Texture, block: &Texture) {
        let mut tile_type;

        let level = level::create_level();

        for i in 0..level.len() {
            for j in 0..level[i].len() {
                tile_type = level[i][j];
                self.draw_tile_isometric(renderer, tile_type, i as i32, j as i32, floor, block);

                /*if hero_map_tile.x == j && hero_map_tile.y == i {*/
                //self.draw_hero_isometric();
                /*}*/

                /*let mut cart_pt = coordinates::Point::new((j * 50) as i32, (i * 50) as i32);*/
                //let wall_height = 103 - 53;
                //let iso_pt = cart_pt.cartesian_to_isometric();
                //if tile_type == 1 {
                //renderer
                //.canvas
                //.copy(
                //&floor,
                //None,
                //Rect::new(iso_pt.x + 250, iso_pt.y + 50 - wall_height, 103, 98),
                //)
                //.unwrap();
                //}
                //if tile_type == 0 {
                //renderer
                //.canvas
                //.copy(
                //&block,
                //None,
                //Rect::new(iso_pt.x + 250, iso_pt.y + 47, 103, 53),
                //)
                //.unwrap();
                //}
            }
        }
    }

    fn draw_tile_isometric(
        &mut self,
        renderer: &mut Renderer,
        tile_type: u8,
        i: i32,
        j: i32,
        floor: &Texture,
        block: &Texture,
    ) {
        let wall_height = FLOOR_GRAPHICS_WIDTH - FLOOR_GRAPHICS_HEIGH;

        let mut iso_pt = coordinates::Point::new(0, 0);
        let mut cart_pt = coordinates::Point::new(0, 0);

        cart_pt.x = j * TILE_WIDTH;
        cart_pt.y = i * TILE_WIDTH;

        iso_pt = cart_pt.cartesian_to_isometric();

        if tile_type == 1 {
            renderer
                .canvas
                .copy(
                    &block,
                    None,
                    Rect::new(
                        iso_pt.x + BORDER_OFFSET.x,
                        iso_pt.y + BORDER_OFFSET.y - wall_height as i32,
                        WALL_GRAPHICS_HEIGH,
                        WALL_GRAPHICS_WIDTH,
                    ),
                )
                .unwrap();
        } else {
            renderer
                .canvas
                .copy(
                    &floor,
                    None,
                    Rect::new(
                        iso_pt.x + BORDER_OFFSET.x,
                        //iso_pt.y + BORRDER_OFFSET.y,
                        iso_pt.y + 47,
                        FLOOR_GRAPHICS_WIDTH,
                        FLOOR_GRAPHICS_HEIGH,
                    ),
                )
                .unwrap();
        }
    }

    fn draw_hero_isometric(&mut self) {
        let iso_pt = coordinates::Point::new(0, 0);
    }
}
