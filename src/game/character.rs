use game::player::*;

pub struct Character {
    pub name: String,
    pub class: String,
    pub health: i32,
    _attack: i32,
    _dodge: i32,
    _luck: i32,
    _xp: i32,
}

impl Player for Character {
    fn new(
        name: String,
        class_name: String,
        health: i32,
        attack: i32,
        dodge: i32,
        luck: i32,
    ) -> Character {
        Character {
            name: name.to_string(),
            class: class_name.to_string(),
            health: health,
            _attack: attack,
            _dodge: dodge,
            _luck: luck,
            _xp: 0,
        }
    }

    fn select(&self, player_name: String, player_luck: i32) -> Self {
        Self::new(
            player_name,
            self.class.to_string(),
            self.health,
            self._attack,
            self._dodge,
            self._luck + player_luck,
        )
    }

    fn damage(&mut self, damage_amount: i32) {
        self.health -= damage_amount;
        self._xp += 2;
    }

    fn heal(&mut self, heal_amount: i32) {
        self.health += heal_amount;
        self._xp += 1;
    }

    fn attack(&self) -> i32 {
        self._xp + self._attack + self._luck / 2
    }

    fn dodge(&self) -> i32 {
        self._xp + self._dodge + self._luck / 2
    }

    fn info(&self) -> String {
        format!(
            "{} \thp: {} attack {} dodge: {} luck: {}",
            self.class, self.health, self._attack, self._dodge, self._luck
        )
    }

    fn stats(&self) -> String {
        format!(
            "{} - hp: {} attack: {} dodge: {} luck: {} experience: {}",
            self.class, self.health, self._attack, self._dodge, self._luck, self._xp
        )
    }
}
