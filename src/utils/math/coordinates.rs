pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Point {
    pub fn new(x: i32, y: i32) -> Point {
        Point { x: x, y: y }
    }

    pub fn set_point(&mut self, x: i32, y: i32) {
        self.x = x;
        self.y = y;
    }

    pub fn cartesian_to_isometric(&mut self) -> Point {
        let mut tmp_pt = Point::new(0, 0);
        tmp_pt.x = self.x - self.y;
        tmp_pt.y = (self.x + self.y) / 2;
        tmp_pt
    }
    pub fn isometric_to_cartesian(&mut self) -> Point {
        let mut tmp_pt = Point::new(0, 0);
        tmp_pt.x = (2 * self.y + self.x) / 2;
        tmp_pt.y = (2 * self.y + self.x) / 2;
        tmp_pt
    }

    pub fn get_tile_coordinates(&mut self, tile_height: i32) -> Point {
        let mut tmp_pt = Point::new(0, 0);
        tmp_pt.x = f32::floor((self.x / tile_height) as f32) as i32;
        tmp_pt.y = f32::floor((self.y / tile_height) as f32) as i32;
        tmp_pt
    }
}
