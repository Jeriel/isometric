#![feature(nll)]

extern crate sdl2;

mod engine;
mod game;
mod utils;

use engine::core::{Engine, GraphicsApi};
use game::gamestate::GameState;

fn main() {
 if let Ok(mut engine) = Engine::init(GraphicsApi::OpenGl) {
        let mut game_state = GameState::create();
        game_state.initialize(&mut engine.renderer);
        engine.run(&mut |dt, mut engine| {
            game_state.update(dt, &mut engine);
        })
    }

}
